#######  for #######
#for letter in 'Python':
#   print '当前字母 :', letter
#
#char letter; str[] = "Python"; int i;
#for(i = 0; i < len(str); i++){
#	letter = str[i];
#	prinf("%c", letter);
#}
#fruits = ['banana', 'apple',  'mango']
#for fruit in fruits:        # 第二个实例
#   print '当前水果 :', fruit
#
#string fruit; fruits[] = ['banana', 'apple',  'mango']
#for(i = 0; i < len(fruits); i++){
#	fruit = fruits[i];
#	prinf("%s", fruit);
#}
#for param in sys.argv:
#	print(param)
#input()	#暂停等待回车

#'r'：读
#'w'：写
#'a'：追加
#'r+' == r+w（可读可写，文件若不存在就报错(IOError)）
#'w+' == w+r（可读可写，文件若不存在就创建）
#'a+' ==a+r（可追加可写，文件若不存在就创建）

#print(str.upper())          # 把所有字符中的小写字母转换成大写字母
#print(str.lower())          # 把所有字符中的大写字母转换成小写字母
#print(str.capitalize())     # 把第一个字母转化为大写字母，其余小写
#print(str.title())          # 把每个单词的第一个字母转化为大写，其余小写 
	
import os
import sys
import time

def conver_str(src_str, step):
	list_l = []
	i = len(src_str)
	while i > 0:
#		print(i)
		list_l.append(src_str[i-step:i])
		i -= step
#		print(list_l)
	s = "".join(list_l)
#	print("conver_str: " + s)
	return s

global MG21_flag
global program_install_code_flag
global program_mac_addr_flag
global add_flag

MG21_flag = 1
program_install_code_flag = 0
program_mac_addr_flag = 0
add_flag = 0

for param in sys.argv:
	print(param)
	if MG21_flag == 1 and -1 != param.find("ip-"):
		MG21_flag = 0
		adapter_ip = param[param.find("ip-")+3:]
	if param == "installcode":
		program_install_code_flag = 1
	if param == "mac_addr":
		program_mac_addr_flag = 1
	if param == "add":
		add_flag = 1
	
print("------------------------------------")	
if MG21_flag == 1:
	print("--> mg21")
else:
	print("--> em3555")
	print("--> adapter ip:" + adapter_ip)
if program_install_code_flag == 1:
	print("--> program_install_code")
if program_mac_addr_flag == 1:
	print("--> program_mac_addr")
if add_flag == 1:
	if program_install_code_flag == 1:
		print("--> install_code add")
	if program_mac_addr_flag == 1:
		print("--> mac_addr add")
print("------------------------------------")
	
work_path = os.getcwd()

###################################################################
#解锁
#print("\n\n---->Unlock")
#cmd = "\"C:\\Program Files (x86)\\Silicon Labs\\ISA3 Utilities\\bin\\em3xx_load.exe\"  --ip " + \
#        adapter_ip + " --disablerdprot "
#em3xx_load --disablerdprot

###################################################################
#烧录InstallCode
if program_install_code_flag == 1 or add_flag == 1:
	print("\n\n---->program InstallCode..")
	if MG21_flag == 1:
		cmd = "C:\\SiliconLabs\\SimplicityStudio\\v4\\developer\\adapter_packs\\commander\\commander.exe flash --device EFR32MG21 --tokengroup znet --tokenfile " + \
				work_path + "\\install_code.txt"
	else:
		cmd = "\"C:\\Program Files (x86)\\Silicon Labs\\ISA3 Utilities\\bin\\em3xx_load.exe\"  --ip " + \
				adapter_ip + " --cibtokenspatch " + work_path + "\\install_code.txt"
	stdout = os.popen(cmd)
	time.sleep(1)
	output = stdout.read()
	print(output)
	if output.find("ERROR") != -1:
		exit("---->program InstallCode error!!!")
	else:
		print("---->program InstallCode done!!!")

###################################################################
#烧录MAC
if program_mac_addr_flag == 1:
	print("\n\n---->program MAC..")
	if MG21_flag == 1:
		cmd = "C:\\SiliconLabs\\SimplicityStudio\\v4\\developer\\adapter_packs\\commander\\commander.exe flash --device EFR32MG21 --tokengroup znet --tokenfile " + \
				work_path + "\\mac_address.txt"
	else:
		cmd = "\"C:\\Program Files (x86)\\Silicon Labs\\ISA3 Utilities\\bin\\em3xx_load.exe\"  --ip " + \
				adapter_ip + " --cibtokenspatch " + work_path + "\\mac_address.txt"
				#If there is a path in a command, 
				#and if that path has a space character in it, 
				#the path must enclosed in double quotation marks.
	stdout = os.popen(cmd)
	time.sleep(1)
	output = stdout.read()
	print(output)
	if output.find("ERROR") != -1:
		exit("---->program MAC error!!!")
	else:
		print("---->program MAC done!!!")

###################################################################
#读取InstallCode MAC
print("\n\n---->read InstallCode and MAC..")
if MG21_flag == 1:
	cmd = "C:\\SiliconLabs\\SimplicityStudio\\v4\\developer\\adapter_packs\\commander\\commander.exe \
			tokendump --device EFR32MG21 --tokengroup znet"
else:
	cmd = "\"C:\\Program Files (x86)\\Silicon Labs\\ISA3 Utilities\\bin\\em3xx_load.exe\"  --ip " + \
			adapter_ip + " --cibtokensprint"
stdout = os.popen(cmd)
time.sleep(1)
output = stdout.read()
print(output)
if output.find("ERROR") != -1:
    exit("---->read InstallCode and MAC error!!!")
else:
    print("---->read InstallCode and MAC done!!!")
output = output.replace('\t', '')
output = output.replace(' ', '')
if MG21_flag == 1:
	len1 = output.find("MFG_CUSTOM_EUI_64")
else:
	len1 = output.find("TOKEN_MFG_CUSTOM_EUI_64")
len1 = output.find(":", len1)
MAC_S = output[len1+1 : len1+1+16]
print("---->MAC:\t\t" + MAC_S)
if MG21_flag == 1:
	len1 = output.find("#InstallCodeFlags:")
	len1 = output.find("InstallCode", len1+len("#InstallCodeFlags:"))
else:
	len1 = output.find("InstallCode[16bytearray]")
len1 = output.find(":", len1)
InstallCode_S = output[len1+1 : len1+1+32]
print("---->InstallCode:\t" + InstallCode_S)
if MG21_flag == 1:
	len1 = output.find("#CRC")
else:
	len1 = output.find("CRC")
len1 = output.find(":", len1)
InstallCode_CRC_S = output[len1+1+2 : len1+1+6]
print("---->InstallCode CRC:\t" + InstallCode_CRC_S)

###################################################################
#保存log
print("\n\n---->save program log..")
if os.path.exists(work_path + "\\program_log.txt") != True:
    f = open(work_path + "\\program_log.txt","w")    #直接打开一个文件，如果文件不存在则创建文件
    f.close()
f = open(work_path + "\\program_log.txt","a+")
if MG21_flag == 1:
	CHIP = "mg21"
else:
	CHIP = "em3555"
f_str = MAC_S[0:2] + ":\t" + conver_str(MAC_S,2) + "  " + InstallCode_S + "  0x" + InstallCode_CRC_S + "\t" + CHIP + "\n"
f.write(f_str)
f.close()
print("---->save program log done!!!")

###################################################################
#更新InstallCode 
if add_flag == 1:
	print("\n\n---->add InstallCode..")
	str_temp = InstallCode_S[30 : 32]
	hex_temp = int(str_temp, 16)
	hex_temp += 1
	
	if hex_temp > (0xff):
		hex_temp = 0
	str_temp = hex(hex_temp)
	if (len(str_temp)) == 3:
		str_temp = str_temp.replace("0x", "0")
	else:
		str_temp = str_temp.replace("0x", "")
	str_temp = str_temp.upper()
	InstallCode_S = InstallCode_S[ : 30] + str_temp
	print("---->new InstallCode:\t" + InstallCode_S)

	f = open(work_path + "\\install_code.txt","r")
	f_str = f.read()
	f.close()
	t_str = f_str.replace('\t', '')
	t_str = t_str.replace(' ', '')
	len1 =  t_str.find("InstallCode:")
	t_str = t_str[(len1+len("InstallCode:")) : (len1+len("InstallCode:")+32)]
	f_str = f_str.replace(t_str, InstallCode_S)
	f = open(work_path + "\\install_code.txt","w")
	f.write(f_str)
	f.close()
	
#更新MAC
if add_flag == 1:
	print("\n\n---->add MAC..")
	str_temp = MAC_S[0 : 2]
	hex_temp = int(str_temp, 16)
	hex_temp += 1
	if hex_temp > (0xff):
		hex_temp = 0
	str_temp = hex(hex_temp)
	if (len(str_temp)) == 3:
		str_temp = str_temp.replace("0x", "0")
	else:
		str_temp = str_temp.replace("0x", "")
	str_temp = str_temp.upper()
	MAC_S = str_temp + MAC_S[2 : ]
	print("---->new MAC:\t\t" + MAC_S)

	f = open(work_path + "\\mac_address.txt","r")
	f_str = f.read()
	f.close()
	t_str = f_str.replace('\t', '')
	t_str = t_str.replace(' ', '')
	len1 =  t_str.find("TOKEN_MFG_CUSTOM_EUI_64:")
	t_str = t_str[(len1+len("TOKEN_MFG_CUSTOM_EUI_64:")) : (len1+len("TOKEN_MFG_CUSTOM_EUI_64:")+16)]
	f_str = f_str.replace(t_str, MAC_S)
	f = open(work_path + "\\mac_address.txt","w")
	f.write(f_str)
	f.close()

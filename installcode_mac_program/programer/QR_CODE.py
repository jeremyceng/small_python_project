import os
import sys

def conver_str(src_str, step):
	list_l = []
	i = len(src_str)
	while i > 0:
#		print(i)
		list_l.append(src_str[i-step:i])
		i -= step
#		print(list_l)
	s = "".join(list_l)
#	print("conver_str: " + s)
	return s
	
image_id = sys.argv[1]
f = open("program_log.txt","r")
f_str = f.read()
f.close()

#print(f_str)
len_tatle = len(f_str)
len1 = 0

while len1 < len_tatle-1:
	len1 = f_str.find("\t", len1)
	len1 = len1+1
	mac_str = f_str[len1 : len1+16]
#	print(mac_str)
	len1 = f_str.find(" ", len1)
	len1 = len1+2
	installcode_str = f_str[len1 : len1+32]
#	print(installcode_str)
	len1 = f_str.find("0x", len1)
	len1 = len1+2
	crc_str = f_str[len1 : len1+4]
	crc_str = conver_str(crc_str, 2)
#	print(crc_str)
	len1 = f_str.find("\n", len1)
	if os.path.exists("qr_code_log.txt") != True:
		f = open("qr_code_log.txt","w")    #直接打开一个文件，如果文件不存在则创建文件
		f.close()
	f = open("qr_code_log.txt","a+")
	rq_code_str = "Z:" + mac_str + "$I:" + installcode_str + crc_str + "%M:1189$D:" + image_id + "011320\n"
	f.write(rq_code_str)
	f.close()

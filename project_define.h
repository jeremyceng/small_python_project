/**
 * @file light_define.h
 * @brief color temperature table
 * @ light definitions
 *
 *
 * <!-- Copyright 2015 by Osram Sylvania. All rights reserved.          *80* -->
 *******************************************************************************
 */
#ifndef __AF_LIGHT_DEFINE__
#define __AF_LIGHT_DEFINE__


#define PROJECT_USE_LEDVANCE_DEFINE_ENALBLED							false
#if (PROJECT_USE_LEDVANCE_DEFINE_ENALBLED == true)
#define PROJECT_MANUFACTURE_ID 											0x1234//0x1189
#define PROJECT_IMAGE_TYPE_ID											0x5678//0x0060
#define PROJECT_FIRMWARE_VERSION 										0xabcd1235//0x00103002
#define PROJECT_HARDWARE_VERSION										0xa5//0x02
#endif

#define PROJECT_MANUFACTURER_NAME										{  8,'L','E','D','V','A','N','C','E',  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0}
#define PROJECT_MODEL_IDENTIFIER										{ 13,'L','E','D','V','A','N','C','E',' ','P','L','U','G',  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0}

#define PROJECT_MF_MODE_ENABLED											true

//
//#define FW_LIGHT_DEFINITION_TOUCHLINK_RSSI_THRESHOLD -50 // this values should be evaluated and tuned for each product to limit touchlink range within 10cm
//

#endif // __AF_LIGHT_DEFINE__


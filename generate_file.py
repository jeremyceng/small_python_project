import os
import time
import sys
import shutil

print('\n')
work_path = os.getcwd()
work_path = work_path.replace("LEDVANCE_Projects", "")
print("working path:\t\t\t" + work_path)
DATE = time.strftime("%Y%m%d")
print("date:\t\t\t\t" + DATE)
TIME = time.strftime("%H%M%S")
print("time:\t\t\t\t" + TIME)

LEDVANCE_Projects = sys.argv[1]
config_o = open(LEDVANCE_Projects).read()
LEDVANCE_Projects = LEDVANCE_Projects.replace(".h", "")

config_o = config_o.replace('\t', '')
config_o = config_o.replace(' ', '')

len1 =  config_o.find("#define__LEDVANCE_ALL_PRODUCTS__")
len1 =  config_o.find("#define", len1+len("#define__LEDVANCE_ALL_PRODUCTS__"))
len2 =  config_o.find("\n", len1)
if len1 == -1 or len2 == -1:
    print("read LEDVANCE_Projects_XXX.h error!!")
    exit("error!!!")

LEDVANCE_Project_Name = config_o[len1:len2]
LEDVANCE_Project_Name = LEDVANCE_Project_Name.replace('#define', '')
LEDVANCE_Project_Name = LEDVANCE_Project_Name.replace('\n', '')
print("current project:\t\t" + LEDVANCE_Projects + "\t" + LEDVANCE_Project_Name)

len1 =  config_o.find("#ifdef"+LEDVANCE_Project_Name)
len2 =  config_o.find("#endif", len1)
if len1 == -1 or len2 == -1:
    print("read LEDVANCE_Projects_XXX.h error!!")
    exit("error!!!")

include_flie = config_o[len1+len("#ifdef"+LEDVANCE_Project_Name):len2]
include_flie = include_flie.replace('#include', '')
include_flie = include_flie.replace('"', '')
include_flie = include_flie.replace('\n', '')
print("include project file:\t\t" + include_flie)

config_o = open(include_flie,"r+").read()

config_o = config_o.replace('\t', '')
config_o = config_o.replace(' ', '')

len1 =  config_o.find("#definePROJECT_USE_LEDVANCE_DEFINE_ENALBLED")
len2 =  config_o.find("\n", len1)
if len1 == -1 or len2 == -1:
    print("read project_define.h error!!")
    exit("error!!!")

enable = config_o[len1+len("#definePROJECT_USE_LEDVANCE_DEFINE_ENALBLED"):len2]

if enable != "true":
    print("LEDVANCE DEFINE NOT ENALBLE!!")
    exit("error!!!")

len1 =  config_o.find("#definePROJECT_MF_MODE_ENABLED")
len2 =  config_o.find("\n", len1)
if len1 == -1 or len2 == -1:
    print("read project_define.h error!!")
    exit("error!!!")

enable = config_o[len1+len("#definePROJECT_MF_MODE_ENABLED"):len2]

if enable == "true":
    print("PROJECT_MF_MODE_ENABLED:\tTRUE")
    PROJECT_MF_MODE_ENABLED = "MF_ENABLE"
else:
    print("PROJECT_MF_MODE_ENABLED:\tFALSE")
    PROJECT_MF_MODE_ENABLED = "MF_DISABLE"
	
len1 =  config_o.find("#definePROJECT_MANUFACTURE_ID")
PROJECT_MANUFACTURE_ID = config_o[len1+len("#definePROJECT_MANUFACTURE_ID"):len1+len("#definePROJECT_MANUFACTURE_ID")+6]
print("PROJECT_MANUFACTURE_ID:\t\t"+PROJECT_MANUFACTURE_ID)
len1 =  config_o.find("#definePROJECT_IMAGE_TYPE_ID")
PROJECT_IMAGE_TYPE_ID = config_o[len1+len("#definePROJECT_IMAGE_TYPE_ID"):len1+len("#definePROJECT_IMAGE_TYPE_ID")+6]
print("PROJECT_IMAGE_TYPE_ID:\t\t"+PROJECT_IMAGE_TYPE_ID)
len1 =  config_o.find("#definePROJECT_FIRMWARE_VERSION")
PROJECT_FIRMWARE_VERSION = config_o[len1+len("#definePROJECT_FIRMWARE_VERSION"):len1+len("#definePROJECT_FIRMWARE_VERSION")+10]
print("PROJECT_FIRMWARE_VERSION:\t"+PROJECT_FIRMWARE_VERSION)
if len1 == -1 or len2 == -1:
    print("read project_define.h error!!")
    exit("error!!!")

print("\n")
print("please wait......")

LEDVANCE_Projects = LEDVANCE_Projects.replace("_Projects", "")
file_name = LEDVANCE_Projects +'_'+ LEDVANCE_Project_Name +'_'+ \
		PROJECT_MANUFACTURE_ID +'_'+ PROJECT_IMAGE_TYPE_ID +'_'+ PROJECT_FIRMWARE_VERSION +'_'+ \
		PROJECT_MF_MODE_ENABLED +'_'+ DATE +'_'+ TIME + "_encrypted"
cmd = "C:\\SiliconLabs\\SimplicityStudio\\v4\\developer\\adapter_packs\\commander\\commander gbl create "+ \
    file_name + ".gbl --app "+ \
    work_path + LEDVANCE_Projects + "\\build\\exe\\" + LEDVANCE_Projects + ".s37 --encrypt "+ \
    work_path + "LEDVANCE_Projects\\" + LEDVANCE_Project_Name + "\\encryption-key"
stdout = os.popen(cmd)
time.sleep(1)
print(".")
time.sleep(1)
print(".")
output = stdout.read()
print(output)
if output.find("ERROR") != -1:
    print("generate gbl file error!!")
    exit("error!!!")

cmd = "C:\\SiliconLabs\\SimplicityStudio\\v4\\developer\\sdks\\gecko_sdk_suite\\v2.5\\protocol\\zigbee\\tool\\image-builder\\image-builder-windows.exe --create "+ \
    file_name + ".ota --version " + PROJECT_FIRMWARE_VERSION + \
    " --manuf-id " + PROJECT_MANUFACTURE_ID + \
    " --image-type " + PROJECT_IMAGE_TYPE_ID + \
    " --tag-id 0x0000 --tag-file " + file_name + ".gbl  --string " + LEDVANCE_Project_Name
stdout = os.popen(cmd)
time.sleep(1)
print(".")
output = stdout.read()
print(output)
if output.find("ERROR") != -1:
    print("generate ota file error!!")
    exit("error!!!")

cmd = "C:\\SiliconLabs\\SimplicityStudio\\v4\\developer\\adapter_packs\\commander\\commander convert " + \
    work_path + LEDVANCE_Projects + "\\build\\exe\\" + LEDVANCE_Projects + ".s37 bootloader-storage-internal-single-512k-mg21-encrypted.s37.ledvance --tokenfile " + \
    work_path + "LEDVANCE_Projects\\" + LEDVANCE_Project_Name + "\\encryption-key --device efr32mg21f512 --tokengroup znet --outfile " + \
    file_name + ".hex"
stdout = os.popen(cmd)
time.sleep(1)
print(".")
output = stdout.read()
print(output)
if output.find("ERROR") != -1:
    print("generate hex file error!!")
    exit("error!!!")

if os.path.exists(LEDVANCE_Project_Name+"_output") != True:
	os.mkdir(LEDVANCE_Project_Name+"_output")
shutil.move(work_path+"LEDVANCE_Projects\\"+file_name+".hex", work_path+"LEDVANCE_Projects\\"+LEDVANCE_Project_Name+"_output\\"+file_name+".hex")
shutil.move(work_path+"LEDVANCE_Projects\\"+file_name+".ota", work_path+"LEDVANCE_Projects\\"+LEDVANCE_Project_Name+"_output\\"+file_name+".ota")
os.remove(work_path+"LEDVANCE_Projects\\"+file_name+".gbl")
	
print("done!!!")
